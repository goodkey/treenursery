package at.badkey.treenursery.tree;

import at.badkey.treenursery.fertilizer.Fertilizer;
import at.badkey.treenursery.CultivationArea;

public abstract class Tree {

	private float maxSize;
	private float maxTrunkDiameter;
	private CultivationArea cultivationArea;
	private Fertilizer fertilizer;
	
	public Tree(CultivationArea cultivationArea, Fertilizer fertilizer, float maxSize, float maxTrunkDiameter) {
		super();
		this.maxSize = maxSize;
		this.maxTrunkDiameter = maxTrunkDiameter;
		this.cultivationArea = cultivationArea;
		this.fertilizer = fertilizer;
		
	}
	
	public Fertilizer getFertilizer() {
		return fertilizer;
	}
	
	
	
}
