package at.badkey.treenursery.tree;

import at.badkey.treenursery.CultivationArea;
import at.badkey.treenursery.fertilizer.Fertilizer;

public class Conifer extends Tree{
	
	public Conifer(CultivationArea cultivationArea, Fertilizer fertilizer, int maxSize, int maxTrunkDiameter) {
		super(cultivationArea, fertilizer, maxSize, maxTrunkDiameter);
	}


}
