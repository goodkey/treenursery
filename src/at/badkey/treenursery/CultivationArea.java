package at.badkey.treenursery;

import java.util.ArrayList;
import at.badkey.treenursery.tree.Tree;

public class CultivationArea {
	
	private String name;
	private float size;
	
	private ArrayList<Tree> trees = new ArrayList<Tree>();
	
	
	public CultivationArea(String name, float size) {
		super();
		this.name = name;
		this.size = size;
	}
	
	public void addTree(Tree tree) {
		trees.add(tree);
	}
	
	public void furtilizeAllTrees() {
		for (Tree tree : trees) {
			tree.getFertilizer().fertilize();
		}
	}
	
	

}
