package at.badkey.treenursery.fertilizer;

public interface Fertilizer {
	
	public void fertilize();

}
