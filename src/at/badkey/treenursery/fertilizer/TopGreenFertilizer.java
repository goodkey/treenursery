package at.badkey.treenursery.fertilizer;

public class TopGreenFertilizer implements Fertilizer{

	@Override
	public void fertilize() {
		System.out.println("Fertilized with TopGreen");
		
	}

}
