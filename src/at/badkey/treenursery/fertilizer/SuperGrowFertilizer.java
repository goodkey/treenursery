package at.badkey.treenursery.fertilizer;

public class SuperGrowFertilizer implements Fertilizer{

	@Override
	public void fertilize() {
		System.out.println("Fertilized with SuperGrow");
		
	}

}
