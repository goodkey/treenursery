package at.badkey.treenursery;

import java.util.ArrayList;

public class Buisnessman {
	
	private String name;
	private ArrayList<CultivationArea> cultivationAreas = new ArrayList<CultivationArea>();

	public Buisnessman(String name) {
		super();
		this.name = name;
	}
	
	public void addCultivationArea(CultivationArea cultivationArea) {
		this.cultivationAreas.add(cultivationArea);
	}
	
	public ArrayList<CultivationArea> getCultivationAreas(){
		return this.cultivationAreas;
	}
	
}
