package at.badkey.treenursery;

import at.badkey.treenursery.fertilizer.SuperGrowFertilizer;
import at.badkey.treenursery.tree.Conifer;

public class TreeNursery {
	
	public static void main(String[] args) {
		Buisnessman buisnessman = new Buisnessman("Julian");
		CultivationArea cultivationArea = new CultivationArea("Area1", 200);
		cultivationArea.addTree(new Conifer(cultivationArea, new SuperGrowFertilizer(), 100, 50));
		buisnessman.addCultivationArea(cultivationArea);
		
		for(CultivationArea area: buisnessman.getCultivationAreas()) {
			area.furtilizeAllTrees();
		}
	}

}
